﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace VKR_VK_parser
{
    static class Program
    {
        private static string KEY1 = "823f42a5823f42a5823f42a57c825268da8823f823f42a5dfadc44ca350a753942ae399";
        private static string KEY2 = "ac7ec1b3ac7ec1b3ac7ec1b313ac1c1c82aac7eac7ec1b3f687310e285365ee0992b2bb";
        private static string KEY3 = "3f1558643f1558643f155864de3f629c3833f153f1558645f83d4d1d282c37fce784fc3";
        private static string KEY4 = "3758425d3758425d3758425dba372f8600337583758425d57cecf9821d5eb454eff6d3c";

        private static string SOURCE_ID = "310384366";

        private static int MAX_DEPTH = 2;


        static void Main(string[] args)
        {
            KeyAccount account1 = new KeyAccount(KEY1);
            KeyAccount account2 = new KeyAccount(KEY2);
            KeyAccount account3 = new KeyAccount(KEY3);
            KeyAccount account4 = new KeyAccount(KEY4);

            VkUser source_user = new VkUser(SOURCE_ID, "Vladislav", "Gordovskii");
            VkPost test_post = new VkPost("57", source_user, "start");


            DatabaseConnection.InitializeConnection("MSI", "VKR_VK", "Admin", "datafather");

            CommandController.AddAccount(account1);
            CommandController.AddAccount(account2);
            CommandController.AddAccount(account3);
            CommandController.AddAccount(account4);

            //CommandGetLikes get_likes = new CommandGetLikes(test_post, 100, 0);
            //CommandController.Execute(get_likes);

            //Console.WriteLine(test_post.FindRelations().Count);




            //StartCheck(source_user, MAX_DEPTH);

            DataExport.ExportFile("test.txt", 5);



            Console.ReadKey();
        }




        private static void FindStep(List<VkObject> source_objects, int depth)
        {
            if (depth <= 0) { return; }

            List<VkObject> next_pool = new List<VkObject>();

            foreach(VkObject vk_object in source_objects)
            {
                List<Relation> relations = vk_object.FindRelations();

                foreach(Relation relation in relations)
                {
                    VkObject add_object = relation.SecondObject;
                    next_pool.Add(add_object);

                    Console.WriteLine("Add object : " + add_object.ID);

                    string add_command = string.Empty;
                    string add_connection = string.Empty;

                    DatabaseRequestCommand check_connection = new DatabaseRequestCommand(String.Format("SELECT * FROM Relations WHERE (first_id='{0}' AND second_id='{1}') OR (first_id='{1}' AND second_id='{0}')", relation.FirstObject, relation.SecondObject), 1);
                    check_connection.Start();

                    if(check_connection.Result.Count == 0)
                    {
                        add_connection = relation.SQLCommand();
                    }

                    DatabaseRequestCommand check_existance = new DatabaseRequestCommand(String.Format("SELECT * FROM VkObjects WHERE id='{0}'", add_object.ID), 1);
                    check_existance.Start();

                    if (check_existance.Result.Count == 0)
                    {
                        if (add_object.GetType() == typeof(VkUser))
                        {
                            string first_name = (add_object as VkUser).FirstName.Replace("'", "''");
                            string second_name = (add_object as VkUser).LastName.Replace("'", "''");

                            Console.WriteLine(String.Format("Depth : {0} | Adding person {1} {2} {3}", MAX_DEPTH - depth, add_object.ID, first_name, second_name));

                            add_command = String.Format("EXEC pr_InsertNewPeople '{0}','{1}','{2}';", add_object.ID, first_name, second_name);
                        }

                        if (add_object.GetType() == typeof(VkPost))
                        {
                            string content = (add_object as VkPost).Content.Replace("'", "''");

                            Console.WriteLine(String.Format("Depth : {0} | Adding post {1}\n{2}\n\n", MAX_DEPTH - depth, add_object.ID, content));

                            add_command = String.Format("EXEC pr_InsertNewPost '{0}','{1}';", add_object.ID, content);
                        }
                    }

                    DatabaseExecuteCommand command = new DatabaseExecuteCommand(add_command + add_connection);
                    command.Start();

                }
            }

            FindStep(next_pool, depth - 1);
        }


        private static void StartCheck(VkUser vk_object, int depth)
        {
            DatabaseExecuteCommand add_source = new DatabaseExecuteCommand(String.Format("EXEC pr_InsertNewPeople '{0}','{1}','{2}'", vk_object.ID, vk_object.FirstName.Replace("'", "''"), vk_object.LastName.Replace("'", "''")));
            add_source.Start();


            FindStep(new List<VkObject>() { vk_object }, MAX_DEPTH);
        }





    }
}
