﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Sql;
using System.IO;

namespace VKR_VK_parser
{
    public static class DataExport
    {


        public static void ExportFile(string file_name, int groups_count)
        {
            StreamWriter writer = new StreamWriter(file_name);

            writer.WriteLine("SOURCE\tTARGET\tBETWEENES\tTYPE\tHUMAN READABLE\tGROUP\tTYPE\tHUMAN READABLE\tGROUP");

            DatabaseRequestCommand result = new DatabaseRequestCommand(String.Format("SELECT * FROM FullRelations({0})", groups_count), 1000000);
            result.Start();

            foreach(Object[] row in result.Result)
            {
                writer.WriteLine(String.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}",row[0],row[1],row[2],row[3],row[4],row[5],row[6],row[7],row[8]));
            }

            writer.Close();
        }
    }
}

