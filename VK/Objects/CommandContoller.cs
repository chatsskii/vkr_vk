﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading;

namespace VKR_VK_parser
{
    public static class CommandController
    {
        private static List<KeyAccount> _accounts = new List<KeyAccount>();

        public static void AddAccount(KeyAccount account)
        {
            _accounts.Add(account);
        }

        private static KeyAccount ChooseAccount()
        {
            _accounts.Sort(new Comparison<KeyAccount>((KeyAccount account_1, KeyAccount account_2) => { return (int)Math.Floor(account_2.RequestsPerSecond - account_1.RequestsPerSecond); }) );

            return _accounts[0];
        }

        public static void Execute(VkCommand command)
        {
            command.Start(ChooseAccount());
        }



    }
}
