﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKR_VK_parser
{
    public class VkUser: VkObject
    {
        private string _first_name;
        private string _last_name;


        public VkUser(string id, string first_name, string last_name) : base(id)
        {
            _id = id;
            _first_name = first_name;
            _last_name = last_name;
        }

        public string FirstName
        {
            get { return _first_name; }
        }

        public string LastName
        {
            get { return _last_name; }
        }

        public string FullName
        {
            get { return FirstName + " " + LastName; }
        }


        private List<VkPost> FindPosts()
        {
            List<VkPost> posts = new List<VkPost>();

            int MAX_POSTS_PER_REQUEST = 100;
            int offset = 0;

            CommandGetPosts posts_command = new CommandGetPosts(this, MAX_POSTS_PER_REQUEST, offset);
            CommandController.Execute(posts_command);


            while (posts_command.ItemsCount > 0)
            {

                List<string> post_ids = posts_command.PostId;
                List<string> post_texts = posts_command.PostText;

                for (int i = 0; i < posts_command.ItemsCount; i++)
                {
                    Console.WriteLine(String.Format("{0} ==> find post {1}\n{2}", ID, post_ids[i], post_texts[i]));
                    posts.Add(new VkPost(post_ids[i], this, post_texts[i]));
                }

                offset += MAX_POSTS_PER_REQUEST;

                posts_command = new CommandGetPosts(this, MAX_POSTS_PER_REQUEST, offset);
                CommandController.Execute(posts_command);
            }

            return posts;
        }

        private List<VkUser> FindFriends()
        {
            List<VkUser> friends = new List<VkUser>();

            int MAX_FRIENDS_PER_REQUEST = 100;
            int offset = 0;

            CommandGetFriends friends_command = new CommandGetFriends(this, MAX_FRIENDS_PER_REQUEST, offset);
            CommandController.Execute(friends_command);


            while (friends_command.ItemsCount > 0)
            {

                List<string> ids = friends_command.ItemsId;
                List<string> first_names = friends_command.ItemsFirstName;
                List<string> last_names = friends_command.ItemsLastName;

                for (int i = 0; i < friends_command.ItemsCount; i++)
                {
                    Console.WriteLine(String.Format("{0} ==> find friend {1} {2} {3}", ID, ids[i], first_names[i], last_names[i]));
                    friends.Add(new VkUser(ids[i], first_names[i], last_names[i]));
                }

                offset += MAX_FRIENDS_PER_REQUEST;

                friends_command = new CommandGetFriends(this, MAX_FRIENDS_PER_REQUEST, offset);
                CommandController.Execute(friends_command);
            }

            return friends;
        }

        public override List<Relation> FindRelations()
        {
            List<Relation> relations = new List<Relation>();

            List<VkPost> posts = FindPosts();

            foreach (VkPost post in posts)
            {
                relations.Add(new Relation(this, post, RelationType.Author));
            }

            List<VkUser> friends = FindFriends();

            foreach (VkUser friend in friends)
            {
                relations.Add(new Relation(this, friend, RelationType.Friends));
            }

            return relations;
        }
    }
}
