﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKR_VK_parser
{
    public class VkPost : VkObject
    {
        private VkUser _author;

        private string _content;
        private string _post_id;


        public VkPost(string post_id, VkUser author, string content) : base(post_id)
        {
            _author = author;
            _content = content;
            _post_id = post_id;

            _id = Author.ID + "_" + post_id;
        }



        public VkUser Author
        {
            get { return _author; }
        }

        public string Content
        {
            get { return _content; }
        }

        public string PostID
        {
            get { return _post_id; }
        }





        private List<VkUser> FindCommentators()
        {

            List<VkUser> commentators = new List<VkUser>();

            int MAX_COMMENTS_PER_REQUEST = 100;
            int offset = 0;

            CommandGetComments get_comments = new CommandGetComments(this, MAX_COMMENTS_PER_REQUEST, offset);
            CommandController.Execute(get_comments);

            while (get_comments.ItemsCount > 0)
            {
                Console.WriteLine(String.Format("{0} ==> find commentators {1} ", ID, get_comments.GetCommentators().Count));
                commentators.AddRange(get_comments.GetCommentators());

                offset += MAX_COMMENTS_PER_REQUEST;
                get_comments = new CommandGetComments(this, MAX_COMMENTS_PER_REQUEST, offset);
                CommandController.Execute(get_comments);
            }



            return commentators;
        }

        private List<VkUser> FindLikes()
        {
            List<VkUser> likes = new List<VkUser>();

            int MAX_LIKES_PER_REQUEST = 100;
            int offset = 0;

            CommandGetLikes get_likes = new CommandGetLikes(this, MAX_LIKES_PER_REQUEST, offset);
            CommandController.Execute(get_likes);

            while (get_likes.ItemsCount > 0)
            {

                List<string> ids = get_likes.LikeID;
                List<string> first_names = get_likes.FirstName;
                List<string> last_names = get_likes.LastName;

                for (int i = 0; i < get_likes.ItemsCount; i++)
                {
                    Console.WriteLine(String.Format("{0} ==> find like {1} {2} {3}", ID, ids[i], first_names[i], last_names[i]));
                    likes.Add(new VkUser(ids[i], first_names[i], last_names[i]));
                }

                offset += MAX_LIKES_PER_REQUEST;

                get_likes = new CommandGetLikes(this, MAX_LIKES_PER_REQUEST, offset);
                CommandController.Execute(get_likes);
            }


            return likes;
        }


        public override List<Relation> FindRelations()
        {
            List<Relation> relations = new List<Relation>();

            List<VkUser> commentators = FindCommentators();
            
            foreach(VkUser commentator in commentators)
            {
                relations.Add(new Relation(this, commentator, RelationType.Commentator));
            }

            List<VkUser> likes = FindLikes();

            foreach(VkUser like in likes)
            {
                relations.Add(new Relation(this, like, RelationType.Like));
            }

            return relations;
        }
    }
}
