﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKR_VK_parser
{
    public abstract class VkObject
    {
        protected string _id;

        public VkObject(string id) { }

        public string ID
        {
            get { return _id; }
        }



        public abstract List<Relation> FindRelations();
    }
}
