﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;

namespace VKR_VK_parser
{
    public abstract class VkCommand
    {
        private string _result = null;
        private bool _isDone = false;

        public abstract void Start(KeyAccount executer);

        protected string LoadJson(string URI)
        {
            string json_document = string.Empty;

            while(true)
            {
                try
                {
                    HttpWebRequest request = HttpWebRequest.CreateHttp(URI);
                    HttpWebResponse response = (HttpWebResponse)request.GetResponse();

                    StreamReader reader = new StreamReader(response.GetResponseStream());
                    json_document = reader.ReadToEnd();
                    break;

                }
                catch { Console.WriteLine(string.Format("Connection error! \n {0}", URI)); Console.ReadKey(); }
            }



            return json_document;
        }

        public string Result
        {
            get { return _result; }
        }

        public bool IsDone
        {
            get { return _isDone; }
        }

        protected void CompleteCommand(string result)
        {
            _result = result;
            _isDone = true;
        }

    }
}
