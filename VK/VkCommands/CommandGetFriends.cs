﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace VKR_VK_parser
{
    public class CommandGetFriends : VkCommand
    {
        private VkUser _person;
        private int _count;
        private int _offset;

        public CommandGetFriends(VkUser person, int count, int offset)
        {
            _person = person;
            _count = count;
            _offset = offset;
        }

        public override void Start(KeyAccount executor)
        {
            string URI = string.Format("https://api.vk.com/method/friends.get?user_id={0}&count={1}&offset={2}&fields=name&access_token={3}&v=5.130", _person.ID, _count, _offset, executor.Key);

            string json_document = LoadJson(URI);

            CompleteCommand(json_document);

        }


        public List<string> GetAttributesByName(string attribute_name)
        {
            List<string> values = new List<string>();

            try
            {

                if (Result == null) { return values; }

                int items_count = JObject.Parse(Result).SelectToken("response.items").Count();

                for (int i = 0; i < items_count; i++)
                {
                    values.Add((string)JObject.Parse(Result).SelectToken(String.Format("response.items[{0}].{1}", i, attribute_name)));
                }
            }
            catch
            {
                Console.WriteLine("Error!");
                Console.WriteLine(Result);
            }

            return values;


        }

        public int ItemsCount
        {
            get
            {
                if (Result == null) { return -1; }

                if (JObject.Parse(Result).SelectToken("response.items") == null) { return 0; }

                return JObject.Parse(Result).SelectToken("response.items").Count();
            }
        }

        public List<string> ItemsId
        {
            get { return GetAttributesByName("id"); }
        }

        public List<string> ItemsFirstName
        {
            get { return GetAttributesByName("first_name"); }
        }

        public List<string> ItemsLastName
        {
            get { return GetAttributesByName("last_name"); }
        }


    }

}
