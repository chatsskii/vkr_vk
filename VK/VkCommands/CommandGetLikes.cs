﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace VKR_VK_parser
{
    public class CommandGetLikes : VkCommand
    {
        private VkPost _post;
        private int _count;
        private int _offset;

        public CommandGetLikes(VkPost post, int count, int offset)
        {
            _post = post;
            _count = count;
            _offset = offset;
        }

        public override void Start(KeyAccount executor)
        {
            string URI = string.Format("https://api.vk.com/method/likes.getList?type=post&owner_id={0}&item_id={1}&extended=1&fields=name&count={2}&offset={3}&access_token={4}&v=5.130", _post.Author.ID, _post.PostID, _count, _offset, executor.Key);

            string json_document = LoadJson(URI);

            CompleteCommand(json_document);
        }

        public int ItemsCount
        {
            get
            {
                if (Result == null) { return -1; }
                if (JObject.Parse(Result).SelectToken("response.items") == null) { return 0; }

                return JObject.Parse(Result).SelectToken("response.items").Count();
            }
        }

        public List<string> GetAttributesByName(string attribute_name)
        {
            List<string> values = new List<string>();

            try
            {

                if (Result == null) { return values; }

                int items_count = JObject.Parse(Result).SelectToken("response.items").Count();

                for (int i = 0; i < items_count; i++)
                {
                    values.Add((string)JObject.Parse(Result).SelectToken(String.Format("response.items[{0}].{1}", i, attribute_name)));
                }
            }
            catch
            {
                Console.WriteLine("Error!");
                Console.WriteLine(Result);
            }

            return values;

        }

        public List<string> LikeID
        {
            get { return GetAttributesByName("id"); }
        }

        public List<string> FirstName
        {
            get { return GetAttributesByName("first_name"); }
        }

        public List<string> LastName
        {
            get { return GetAttributesByName("last_name"); }
        }
    }

}
