﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace VKR_VK_parser
{
    public class KeyAccount
    {
        private static int MAX_REQUESTS_PER_SECOND = 3;

        private string _key;

        private List<long> _request_times = new List<long> { 0 };

        public KeyAccount(string key)
        {
            _key = key;
        }

        private long Milliseconds
        {
            get { return DateTime.Now.Ticks / TimeSpan.TicksPerMillisecond; }
        }

        private void AddTime()
        {
            while (_request_times.Count >= MAX_REQUESTS_PER_SECOND) { _request_times.RemoveAt(0); }

            _request_times.Add(Milliseconds);
        }

        public void Execute(VkCommand command)
        {
            command.Start(this);
            AddTime();
        }

        public string Key
        {
            get { return _key; }
        }

        public bool Available
        {
            get { return RequestsPerSecond < MAX_REQUESTS_PER_SECOND; }
        }

        public float RequestsPerSecond
        {
            get
            {

                long start_time = _request_times[0];

                long miliseconds = Milliseconds - start_time;

                foreach(long time in _request_times)
                {
                    miliseconds += time - start_time;
                }

                float seconds = miliseconds / 1000;

                return (_request_times.Count + 1) / seconds;
                    
            }
        }


    }
}
