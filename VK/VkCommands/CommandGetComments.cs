﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace VKR_VK_parser
{
    public class CommandGetComments : VkCommand
    {
        private VkPost _post;
        private int _count;
        private int _offset;

        public CommandGetComments(VkPost post, int count, int offset)
        {
            _post = post;
            _count = count;
            _offset = offset;
        }

        public override void Start(KeyAccount executor)
        {
            string URI = string.Format("https://api.vk.com/method/wall.getComments?owner_id={0}&post_id={1}&extended=1&fields=name&count={2}&offset={3}&access_token={4}&v=5.130", _post.Author.ID, _post.PostID, _count, _offset, executor.Key);

            string json_document = LoadJson(URI);

            CompleteCommand(json_document);
        }

        public int ItemsCount
        {
            get
            {
                if (Result == null) { return -1; }
                if (JObject.Parse(Result).SelectToken("response.items") == null) { return 0; }

                return JObject.Parse(Result).SelectToken("response.items").Count();
            }
        }

        public List<string> GetAttributesByName(string attribute_name)
        {
            List<string> values = new List<string>();

            try
            {

                if (Result == null) { return values; }

                int items_count = JObject.Parse(Result).SelectToken("response.items").Count();

                for (int i = 0; i < items_count; i++)
                {
                    values.Add((string)JObject.Parse(Result).SelectToken(String.Format("response.items[{0}].{1}", i, attribute_name)));
                }
            }
            catch
            {
                Console.WriteLine("Error!");
                Console.WriteLine(Result);
            }

            return values;

        }

        public List<VkUser> GetCommentators()
        {

            List<VkUser> commentators = new List<VkUser>();

            if (Result == null) { return commentators; }




            int count = JObject.Parse(Result).SelectToken(String.Format("response.profiles")).ToArray().Length;

            for (int i = 0; i < count; i++)
            {

                string id = (string)JObject.Parse(Result).SelectToken(String.Format("response.profiles[{0}].id", i));
                string first_name = (string)JObject.Parse(Result).SelectToken(String.Format("response.profiles[{0}].first_name", i));
                string last_name = (string)JObject.Parse(Result).SelectToken(String.Format("response.profiles[{0}].last_name", i));

                commentators.Add(new VkUser(id, first_name, last_name));

            }

            return commentators;

        }

        public List<string> CommentID
        {
            get { return GetAttributesByName("id"); }
        }

        public List<string> CommentText
        {
            get { return GetAttributesByName("text"); }
        }
    }

}
