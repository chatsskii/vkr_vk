﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace VKR_VK_parser
{
    public class Relation
    {
        VkObject _first_object;
        VkObject _second_object;

        RelationType _relation_type;

        public Relation(VkObject first_object, VkObject second_object, RelationType relation_type)
        {
            _first_object = first_object;
            _second_object = second_object;
            _relation_type = relation_type;
        }

        public VkObject FirstObject
        {
            get { return _first_object; }
        }

        public VkObject SecondObject
        {
            get { return _second_object; }
        }

        public RelationType Type
        {
            get { return _relation_type; }
        }

        public string SQLCommand()
        {
            string type = string.Empty;

            switch (this.Type)
            {
                case RelationType.Friends: type = "0"; break;

                case RelationType.Author: type = "1"; break;

                case RelationType.Commentator: type = "2"; break;

                case RelationType.Like: type = "3"; break;
            }

            return String.Format("EXEC pr_InsertRelation '{0}','{1}',{2}", FirstObject.ID, SecondObject.ID, type);


        }

    }
}
