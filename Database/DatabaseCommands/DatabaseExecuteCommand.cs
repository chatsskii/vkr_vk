﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace VKR_VK_parser
{
    public class DatabaseExecuteCommand : DatabaseCommand
    {
        public DatabaseExecuteCommand(string command) : base(command) { }


        public override void Start()
        {
            ExecuteCommand(Command);
            CompleteCommand();
        }

        public static void ExecuteCommand(string command)
        {
            Connection.Open();


            SqlCommand sql_command = new SqlCommand(command, Connection);
            sql_command.CommandTimeout = 0;
            sql_command.ExecuteScalar();


            Connection.Close();
        }

    }
}
