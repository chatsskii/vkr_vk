﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace VKR_VK_parser
{
    public abstract class DatabaseCommand
    {
        private string _command;
        private bool _isDone = false;

        public DatabaseCommand(string command)
        {
            _command = command;
        }

        public abstract void Start();

        public bool IsDone
        {
            get { return _isDone; }
        }

        public string Command
        {
            get { return _command; }
        }

        protected void CompleteCommand()
        {
            _isDone = true;
        }

        protected static SqlConnection Connection
        {
            get { return DatabaseConnection.Connection; }
        }

    }


}

