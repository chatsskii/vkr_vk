﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;

namespace VKR_VK_parser
{
    public class DatabaseRequestCommand : DatabaseCommand
    {
        private List<object[]> _result = null;
        private int _max_rows;


        public DatabaseRequestCommand(string command, int max_rows) : base(command)
        {
            _max_rows = max_rows;
        }

        public override void Start()
        {
            _result = ExecuteCommand(Command, MaxRows);
            CompleteCommand();
        }

        public int MaxRows
        {
            get { return _max_rows; }
        }

        public List<object[]> Result
        {
            get { return _result; }
        }

        private static List<object[]> ExecuteCommand(string command, int max_rows)
        {


            Connection.Open();

            List<object[]> result = new List<object[]>();


            SqlCommand sql_command = new SqlCommand(command, Connection);
            sql_command.CommandTimeout = 0;

            SqlDataReader answer = sql_command.ExecuteReader();

            int row_number = 0;

            while (answer.Read() && row_number < max_rows)
            {
                int fields_count = answer.FieldCount;

                result.Add(new object[fields_count]);

                for (int field = 0; field < fields_count; field++)
                {
                    result[row_number][field] = answer.GetValue(field);
                }

                row_number++;

            }



            Connection.Close();


            return result;
        }

    }
}
