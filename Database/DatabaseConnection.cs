﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Data.SqlClient;
using System.Threading;

namespace VKR_VK_parser
{
    public static class DatabaseConnection
    {
        private static SqlConnection _database_connection;

        public static void InitializeConnection(string server, string data_base, string login, string password)
        {
            _database_connection = new SqlConnection();
            _database_connection.ConnectionString = string.Format("Data Source={0};Initial Catalog={1};User id={2};Password={3}", server, data_base, login, password);
        }

        public static SqlConnection Connection
        {
            get { return _database_connection; }
        }

    }
}
